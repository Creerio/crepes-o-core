/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { Component, parseNameToLower } = require('../../class/Component');
const { GatewayIntentBits, ButtonBuilder } = require('discord.js');
const { ButtonStyle } = require('discord-api-types/v10');
const { getCommands } = require('../../modules/InteractionComponents');

const name = parseNameToLower(__filename);

const builder = new ButtonBuilder()
    .setCustomId(name)
    .setStyle(ButtonStyle.Primary); // Label should be completed by command using the user's language

async function pingAgain(interaction) {
    await getCommands().get('ping').mainFunction(interaction);
}

const btn = new Component({
    name: name,
    intents: [GatewayIntentBits.Guilds],
    builder: builder,
    mainFunction: pingAgain,
    cooldown: 5,
});

module.exports = btn;