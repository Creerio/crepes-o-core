/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { Sequelize } = require('sequelize');
const { logger } = require('./Logger');
const fs = require('fs');
const path = require('path');
const Translator = require('./Translation');

// Fix PG date handling
try {
    const pg = require('pg');
    pg.types.setTypeParser(1114, function(stringValue) {
        return new Date(stringValue.substring(0, 10) + 'T' + stringValue.substring(11) + 'Z');
    });
    logger.debug(Translator.getString('core.dbPgFixed'));
}
catch (e) {
    logger.debug(Translator.getString('core.dbPgSkipped'));
}

/**
 * Database
 */
class Database {
    constructor() {
        this.db = null;
    }

    /**
     * Creates the database connector for the program
     */
    async createDatabaseConnector() {
        // Sqlite used or nothing? Need some specific information
        if (process.env.DB_DIALECT === 'sqlite' || typeof (process.env.DB_DIALECT) != 'string') {
            this.db = new Sequelize({
                dialect: 'sqlite',
                storage: process.env.DB_PATH ? process.env.DB_PATH : 'database.db', // Use database.db as default storage for the database in case it's not defined
                logging: msg => logger.debug(msg),
            });
        }
        else {
            if (!process.env.DB_HOST || !process.env.DB_PORT || !process.env.DB_NAME || !process.env.DB_USERNAME || !process.env.DB_PASSWORD) {
                logger.error(Translator.getString('core.dbMissingInfo'));
                process.exit(4);
            }
            // Use info to create database connector
            this.db = new Sequelize({
                dialect: process.env.DB_DIALECT,
                username: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_NAME,
                host: process.env.DB_HOST,
                port: process.env.DB_PORT,
                logging: msg => logger.debug(msg),
            });
        }

        // Store models
        let testTables;
        for (const model of Object.keys(getModelsFromFolder(this.db))) {
            this.db.modelManager.addModel(model);
            testTables = this.db.modelManager.getModel(model);
            try {
                testTables = await testTables.findAll();
            }
            catch (err) {
                logger.error(Translator.getString('core.dbGetTableError'));
                process.exit(5);
            }
        }

    }

    /**
     * Connect to the database
     */
    connectToDB() {
        this.db.authenticate()
            .then(() => {
                logger.info(Translator.getString('core.dbConnectOk'));
            })
            .catch(err => {
                logger.error(Translator.getString('core.dbConnectError'));
                logger.error(err);
                process.exit(6);
            });
    }
}

/**
 * Returns all models from the database/models folder
 *
 * @param sequelizeInstance
 * The sequelize instance
 *
 * @returns {{}}
 */
function getModelsFromFolder(sequelizeInstance) {
    const models = {};
    fs
        .readdirSync(__dirname + '/../database/models')
        .filter(file => {
            return (file.indexOf('.') !== 0) && (!file.endsWith('index.js')) && (file.slice(-3) === '.js');
        })
        .forEach(file => {
            const model = require(path.join(__dirname + '/../database/models/', file))(sequelizeInstance, require('sequelize').DataTypes);
            models[model.name] = model;
        });
    Object.keys(models).forEach(modelName => {
        if (models[modelName].associate) {
            models[modelName].associate(models);
        }
    });
    return models;
}

// Singleton object
const databaseInstance = new Database();

// Export the logger
module.exports = { databaseInstance, getModelsFromFolder };
