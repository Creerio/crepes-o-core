'use strict';

const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../db_config.js')[env];
const { getModelsFromFolder } = require('../../class/Database');

let sequelize;
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
}
else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
}

const db = getModelsFromFolder(sequelize);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
