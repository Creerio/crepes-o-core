/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { EmbedBuilder } = require('discord.js');
const Translator = require('../class/Translation');

/**
 * Returns an embed with a thumbnail, an author and a timestamp
 * @param client
 * Bot client
 * @returns {EmbedBuilder}
 * Embed formatted as required
 */
function baseEmbed(client) {
    const avatarUrl = client.user.displayAvatarURL({ size: 2048 });
    return new EmbedBuilder()
        .setAuthor({ name: client.user.username, iconURL: avatarUrl, url: avatarUrl })
        .setTimestamp();
}

/**
 * Returns an error embed using baseEmbed as a base
 * @see baseEmbed
 * @param client
 * Bot client
 * @param errorMessage
 * Error message for the user
 * @returns {EmbedBuilder}
 * Embed formatted as required
 */
function errorEmbed(client, errorMessage) {
    return baseEmbed(client)
        .setColor([255, 0, 0])
        .setTitle(Translator.getString('core.error'))
        .setDescription(errorMessage);
}

module.exports = { baseEmbed, errorEmbed };