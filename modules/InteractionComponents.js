/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { GatewayIntentBits, Collection } = require('discord.js');
const path = require('path');
const fs = require('fs');
const { logger } = require('../class/Logger');
const Translator = require('../class/Translation');

// Command storage
const commands = new Collection();
const commandBuilders = [];
const botIntents = [];

// Button storage
const buttons = new Collection();

function loadComponents() {
    // Clear builders & intents before loading components
    commandBuilders.splice(0, commandBuilders.length);
    botIntents.splice(0, commandBuilders.length);

    loadCommands();
    loadButtons();
}

/**
 * Updates local variables commandBuilders & botIntents using data from files
 * Also loads commands (and reloads them too)
 */
function loadCommands() {
    const commandsPath = path.join(__dirname, '../components/commands');

    // No commands folder?
    if (!fs.existsSync(commandsPath)) {
        logger.warn(Translator.getString('core.cmdNoCmds'));
        return;
    }

    // Clear current commands
    commands.clear();

    // Get commands files
    const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

    // Register every single one of them
    logger.debug(Translator.getString('core.cmdReadPath', { 'cmdPath' : commandsPath }));
    for (const file of commandFiles) {
        const filePath = path.join(commandsPath, file);
        const command = require(filePath);
        if (command.disabled) {
            logger.debug(Translator.getString('core.cmpReadFileDisabled', { 'file' : file }));
        }
        else {
            logger.debug(Translator.getString('core.cmpReadFile', { 'file' : file }));

            // Stock command
            commands.set(command.name, command);

            // Command options may need an autocomplete
            let checkAutocomplete = false;
            command.builder.options.map(option => checkAutocomplete = checkAutocomplete || option.autocomplete);
            if ((!checkAutocomplete && command.autocompleteFunction) || (checkAutocomplete && !command.autocompleteFunction)) {
                logger.error(Translator.getString('core.cmdAutocompleteError', { 'cmdName' : command.name }));
                process.exit(7);
            }

            // Stock command builder for the API
            commandBuilders.push(command.builder);

            // No duplicate intents in storage
            botIntents.push(...command.intents.filter((intent) => !botIntents.includes(intent)));
        }
    }
}

/**
 * Updates botIntents using data from files
 * Also loads buttons (and reloads them too)
 */
function loadButtons() {
    const btnPath = path.join(__dirname, '../components/buttons');

    // No buttons folder found. Unlike commands this should be fine
    if (!fs.existsSync(btnPath)) {
        return;
    }

    // Clear buttons
    buttons.clear();

    // Get button files
    const btnFiles = fs.readdirSync(btnPath).filter(file => file.endsWith('.js'));

    // Register them
    logger.debug(Translator.getString('core.btnReadPath', { 'btnPath' : btnPath }));
    for (const file of btnFiles) {
        const filePath = path.join(btnPath, file);
        const btn = require(filePath);

        // A button cannot be disabled.
        // No need to store its builder too
        // It also cannot be autocompleted

        logger.debug(Translator.getString('core.cmpReadFile', { 'file' : file }));

        // Stock command
        buttons.set(btn.name, btn);

        // No duplicate intents in storage
        botIntents.push(...btn.intents.filter((intent) => !botIntents.includes(intent)));
    }
}

/**
 * Returns currently stored commands
 * @returns {*}
 */
function getCommands() {
    return commands;
}

/**
 * Returns currently stored buttons
 * @returns {*}
 */
function getButtons() {
    return buttons;
}

/**
 * Returns all command builders. Can be empty
 * @returns {*[]}
 */
function getCommandsBuilders() {
    return commandBuilders.map(command => command.toJSON());
}

/**
 * Gives all necessary intents, depending on the commands
 * @returns {GatewayIntentBits.Guilds[]|*[]}
 * Either the required intents list or just GatewayIntentBits.Guilds
 */
function getIntents() {
    if (botIntents.length === 0) {
        return [GatewayIntentBits.Guilds];
    }
    else {
        return botIntents;
    }
}

module.exports = { loadComponents, loadCommands, loadButtons, getCommands, getButtons, getCommandsBuilders, getIntents };