/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { logger } = require('../class/Logger');

/**
 * Prints out the license to the user, using the logger
 */
function printLicense() {
    logger.warn('\n\n' +
        'Crepes O Core - Copyright (C) 2022-2023 Creerio\n' +
        'This program comes with ABSOLUTELY NO WARRANTY\n' +
        'This is free software, and you are welcome to redistribute it under certain conditions.\n' +
        'Please refer to the LICENSE file for more details.\n\n\n');
}

module.exports = printLicense;