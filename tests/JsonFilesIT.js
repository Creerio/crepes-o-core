/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { describe } = require('mocha');

const assert = require('assert');
const fs = require('fs');
const jsonFiles = require('../modules/JsonFiles');

// Test values
const folder = jsonFiles.confFolder;
const file = 'test.json';
const fileUrl = folder + file;
const val = {};
val['test'] = 'Hello world !';

describe('Test jsonFile module', function() {

    // eslint-disable-next-line no-undef
    it('checkExistence | Folder does not exist | Using "jaaj/"', function() {
        const res = jsonFiles.checkExistence('jaaj/');
        assert(res === false);
    });

    // eslint-disable-next-line no-undef
    it('checkExistence | Folder should exist | Using "../tests/"', function() {
        const res = jsonFiles.checkExistence('../tests/');
        assert(res === false);
    });

    // eslint-disable-next-line no-undef
    it('createDir | Using previously tested checkExistence', function() { // Will fail if current folder only allow reading or allows nothing
        jsonFiles.createDir(folder);
        assert(jsonFiles.checkExistence(folder) === true);
    });

    // eslint-disable-next-line no-undef
    it('createJsonFile | Using previously tested checkExistence', function() { // Will fail if current folder only allow reading or allows nothing
        jsonFiles.createJsonFile(fileUrl);
        assert(jsonFiles.checkExistence(fileUrl) === true);
    });

    // eslint-disable-next-line no-undef
    it('writeJsonFile | Should write data inside the json, destroys the old json\'s data ', function() {
        const resW = jsonFiles.writeJsonFile(fileUrl, val);
        assert(resW === true); // Will fail if current folder only allow reading or allows nothing
    });

    // eslint-disable-next-line no-undef
    it('readJsonFile | Should read data from inside the json, the data must be the same as the one declared', function() {
        const resR = jsonFiles.readJsonFile(fileUrl);
        assert(resR['test'] === val['test']); // Will fail if current folder doesn't allow reading
    });

    // eslint-disable-next-line no-undef
    it('writeData | Should write specific data inside the json while keeping other data', function() {
        const resW = jsonFiles.writeData(null, 'test', 'test2', 'Hi');
        assert(resW === true); // Will fail if current folder only allow reading or allows nothing
    });

    // eslint-disable-next-line no-undef
    it('readData | Should read data from inside the json, also tests if the old one is still here', function() {
        let resR = jsonFiles.readData(null, 'test', 'test');
        assert(resR === val['test']); // Will fail if current folder doesn't allow reading
        resR = jsonFiles.readData(null, 'test', 'test2');
        assert(resR === 'Hi'); // Will fail if current folder doesn't allow reading
    });

    // eslint-disable-next-line no-undef
    after(function() {
        // Removes any created files
        fs.rmSync(fileUrl);
        fs.rmdirSync(folder);
    });
});
